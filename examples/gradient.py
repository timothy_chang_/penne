"""
Deep recurrent language model. This one stacks RNNs using the
recurrent.Stacked class, which is analogous to composition of
finite-state transducers.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
from penne import compute
import operator
#import penne
import numpy
import random
import time

#use_gpu('cuda0')

hidden_dims = 100  # like the paper
depth = 2

#train = lm.read_data("../data/inferno.en")#[:100]
train = lm.read_data("../data/ptb.train.txt")[:70]
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")#[:100]
valid = lm.read_data("../data/ptb.valid.txt")[:70]

layers = [recurrent.Map(Layer(-len(vocab), hidden_dims, f=None, bias=None))]
for i in xrange(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stack(*layers)

def make_network(words):
    loss = constant(0.)
    prev_w = numberizer.numberize("<s>")
    rnn.start()
    for word in words:
        w = numberizer.numberize(word)
        o = rnn.step(prev_w)
        loss -= o[w]
        prev_w = w
    return loss

load_model("rnnlm1.npy")

loss = make_network(valid[0])
a = []
# loop on hidden units, compute_g for each
for i in xrange(hidden_dims):
	g = compute_gradients(rnn.lastval[-2][i])  # -2 denotes last hidden layer
	# dot(w,g(wx[k])) where w is w of input layer (layers[0].weight[0])
	fweight = layers[0].f.weight[0]  # weight[0] is list
	wx = g[layers[0].f.wx[0]]
	# a.append(numpy.dot(fweight, wx)) # convert to numpyarray
	a.append([numpy.dot(fweight[j].value, wx) for j in xrange(len(vocab))]) 
# grad = compute_gradients(loss, compute_all=True)

for i in xrange(len(a)):
	# for j in xrange(len(vocab)):
		# print numberizer.denumberize(j), a[i][j]
	sort_a = sorted(enumerate(a[i]), key=operator.itemgetter(1))
	rev_a = sorted(sort_a, key=operator.itemgetter(1), reverse=True)
	print "top 10"
	for k in xrange(10):
		print numberizer.denumberize(sort_a[k][0]), sort_a[k][1]
	print "bottom 10"
	for k in xrange(10):
		print numberizer.denumberize(rev_a[k][0]), rev_a[k][1]




# -pe smp 16

"""
Another implementation of a deep recurrent language model. This one
computes the output softmax layer for all the words in a sentence at once.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time

hidden_dims = 100
depth = 1

train = lm.read_data("../data/inferno.en")[:10]
#train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

valid = lm.read_data("../data/purgatorio.en")[:10]
#valid = lm.read_data("../data/ptb.valid.txt")

layers = [recurrent.LSTM(-len(vocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
rnn = recurrent.Stack(*layers)
output_layer = Layer(hidden_dims, len(vocab), f=logsoftmax)

def make_network(words):
    nums = [numberizer.numberize(word) for word in ["<s>"]+words]

    # Array of predictions
    output = output_layer(stack(rnn.transduce(nums[:-1])))

    # Array of correct answers
    correct = stack([one_hot(len(vocab), num) for num in nums[1:]])

    return -dot(reshape(correct, (-1,)), reshape(output, (-1,)))

load_model("rnnlm2.npy")

grad = compute_gradient(loss, compute_all=True)

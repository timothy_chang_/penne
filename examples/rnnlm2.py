"""
Another implementation of a deep recurrent language model. This one
computes the output softmax layer for all the words in a sentence at once.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time

hidden_dims = 100
depth = 1

train = lm.read_data("../data/inferno.en")[:70]
#train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

valid = lm.read_data("../data/purgatorio.en")[:70]
#valid = lm.read_data("../data/ptb.valid.txt")

layers = [recurrent.LSTM(-len(vocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
rnn = recurrent.Stack(*layers)
output_layer = Layer(hidden_dims, len(vocab), f=logsoftmax)

def make_network(words):
    nums = [numberizer.numberize(word) for word in ["<s>"]+words]

    # Array of predictions
    output = output_layer(stack(rnn.transduce(nums[:-1])))

    # Array of correct answers
    correct = stack([one_hot(len(vocab), num) for num in nums[1:]])

    return -dot(reshape(correct, (-1,)), reshape(output, (-1,)))

learning_rate = 0.1
trainer = Adagrad(learning_rate=learning_rate)
prev_valid_ppl = numpy.infty

for epoch in xrange(100):
    start_time = time.time()
    random.shuffle(train)

    train_loss = 0.
    train_size = 0
    for words in train:
        loss = make_network(words)
        train_loss += trainer.receive(loss)
        train_size += len(words)
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for words in valid:
        loss = make_network(words)
        valid_loss += compute_value(loss)
        valid_size += len(words)
    valid_ppl = numpy.exp(valid_loss/valid_size)
    # monitor learning rate
    if valid_ppl >= prev_valid_ppl:
	    learning_rate *= 0.5
	    trainer.learning_rate = learning_rate
    prev_valid_ppl = valid_ppl
    if learning_rate < 0.009:
	    save_model("rnnlm2.npy")
	    print "done"
	    break

    epoch_time = time.time() - start_time
    print "epoch=%s time=%s speed=%s train=%s valid=%s" % (epoch, epoch_time, (train_size+valid_size)/epoch_time, train_ppl, valid_ppl)

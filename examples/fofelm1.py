"""
Deep recurrent language model with a FOFE layer. This one stacks RNNs using the
recurrent.Stacked class, which is analogous to composition of
finite-state transducers.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time
sys.path.append("/afs/crc.nd.edu/group/nlp/02/tchang/penne/penne")
hidden_dims = 400  # 400 nodes per hidden layer?
depth = 2  # like the paper

#train = lm.read_data("../data/inferno.en")#[:1000]
train = lm.read_data("../data/ptb.train.txt")#[:1000]
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")#[:1000]
valid = lm.read_data("../data/ptb.valid.txt")#[:1000]

layers = [recurrent.FOFE(-len(vocab),hidden_dims, 0.7)]  # , recurrent.Map(Sparse_Layer(len(vocab), hidden_dims))]  # projection layer
for i in xrange(depth):
    layers.append(recurrent.Map(Layer(hidden_dims, hidden_dims)))  # the hidden layers. Just a simple tanh I think.
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))  # decoding layer
rnn = recurrent.Stack(*layers)

def make_network(words):
    loss = constant(0.)
    prev_w = numberizer.numberize("<s>")
    rnn.start()
    for word in words:
        w = numberizer.numberize(word)
        o = rnn.step(prev_w)
        loss -= o[w]
        prev_w = w
    return loss

trainer = Adagrad(learning_rate=0.1)


for epoch in xrange(100):
    start_time = time.time()
    random.shuffle(train)

    train_loss = 0.
    train_size = 0
    for words in train:
        loss = make_network(words)
        train_loss += trainer.receive(loss)
        train_size += len(words)
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for words in valid:
        loss = make_network(words)
        valid_loss += compute_value(loss)
        valid_size += len(words)
    valid_ppl = numpy.exp(valid_loss/valid_size)

    epoch_time = time.time() - start_time
    print "epoch=%s time=%s speed=%s train=%s valid=%s" % (epoch, epoch_time, (train_size+valid_size)/epoch_time, train_ppl, valid_ppl)
    sys.stdout.flush()

"""
Deep recurrent language model. This one stacks RNNs using the
recurrent.Stacked class, which is analogous to composition of
finite-state transducers.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
#import penne
import numpy
import random
import time

#use_gpu('cuda0')

hidden_dims = 100  # like the paper
depth = 2

#train = lm.read_data("../data/inferno.en")#[:100]
train = lm.read_data("../data/ptb.train.txt")[:70]
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")#[:100]
valid = lm.read_data("../data/ptb.valid.txt")[:70]

layers = [recurrent.Map(Layer(-len(vocab), hidden_dims, f=None, bias=None))]
for i in xrange(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stack(*layers)

def make_network(words):
    loss = constant(0.)
    prev_w = numberizer.numberize("<s>")
    rnn.start()
    for word in words:
        w = numberizer.numberize(word)
        o = rnn.step(prev_w)
        loss -= o[w]
        prev_w = w
    return loss

learning_rate = 0.1
trainer = Adagrad(learning_rate=learning_rate)
prev_valid_ppl = numpy.infty

for epoch in xrange(100):
    start_time = time.time()
    random.shuffle(train)

    train_loss = 0.
    train_size = 0
    for words in train:
        loss = make_network(words)
        train_loss += trainer.receive(loss)
        train_size += len(words)
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for words in valid:
        loss = make_network(words)
        valid_loss += compute_value(loss)
        valid_size += len(words)
    valid_ppl = numpy.exp(valid_loss/valid_size)
    # monitor learning rate
    if valid_ppl >= prev_valid_ppl:
	    learning_rate *= 0.5
	    trainer.learning_rate = learning_rate
    prev_valid_ppl = valid_ppl
    if learning_rate < 0.009:
	    save_model("rnnlm1.npy")
	    print "done"
	    break

    epoch_time = time.time() - start_time
    print "epoch=%s time=%s speed=%s train=%s valid=%s rate=%s" % (epoch, epoch_time, (train_size+valid_size)/epoch_time, train_ppl, valid_ppl, learning_rate)
    sys.stdout.flush() 
    



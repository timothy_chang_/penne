#!/bin/sh
#$ -N LM1_test
. $HOME/.bashrc
export PYTHONPATH=/afs/crc.nd.edu/group/nlp/02/tchang/penne/penne:$PYTHONPATH
export PATH=/afs/crc.nd.edu/group/nlp/02/tchang/penne/penne:$PATH
# export MKL_NUM_THREADS=$NSLOTS

fsync tests/LM1_test.log &
module load python/2.7.8
exec >tests/LM1_test.log 2>&1
date
python rnnlm1.py
date

#!/bin/sh
#$ -N wsj_400u_all
. $HOME/.bashrc
export PYTHONPATH=/afs/crc.nd.edu/group/nlp/02/tchang/penne/devel/penne:$PYTHONPATH
# export MKL_NUM_THREADS=$NSLOTS
fsync wsj_400u_all.log &
module load python/2.7.8
exec >wsj_400u_all.log 2>&1
date
python fofelm1.py
date

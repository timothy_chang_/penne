"""
Gradient wrt Input History

1. Loads rnnlm1.py
2. Run lm on up to a sentence
3. Calculates gradient dh_i/dw_k, where 
	h_i = hidden unit i in last hidden layer
	w_k = word k in the sentence w
4. Populate txt file with matrix where M[i,k] is how strongly word k affects unit i
"""

"""
0. Exact code as rnnlm1
"""
import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
from penne import compute
from penne import expr
import operator
#import penne
import numpy
import random
import time

hidden_dims = 100  # like the paper
depth = 1

#train = lm.read_data("../data/inferno.en")#[:100]
train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")#[:100]
valid = lm.read_data("../data/ptb.valid.txt")

layers = [recurrent.Map(Layer(-len(vocab), hidden_dims, f=None, bias=None))]
for i in xrange(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stack(*layers)

in_history = []
in_weight = []
def make_network(words):
    loss = constant(0.)
    prev_w = numberizer.numberize("<s>")
    rnn.start()
    for word in words:
        w = numberizer.numberize(word)
        o = rnn.step(prev_w)
	# append h^n_t_j at the jth timestep
	in_history.append(layers[0].f.wx[-1])
	in_weight.append(layers[0].f.weight[0])
        loss -= o[w]
        prev_w = w
    return loss

learning_rate = 0.1

"""
1. Loads rnnlm1.py
"""
print "loading model"
load_model("lm/rnnlm1_1_ptb.npy")
print "done loading model"

"""
2. Run lm on up to a sentence
"""
f = open("data/inp-hist_1_-1.txt", "w")  # our output file
text = ["<s>"] + valid[1][:-2] # input text
loss = make_network(text)  

"""
3. Calculates gradient dh_i/dw_k, where 
	h_i = hidden unit i in last hidden layer
	w_k = word k in the sentence w
"""
for index, k in enumerate(text):
	di_dk = []  # what unit i thinks of entire vocab when procdssing word k
	out_data = []
	w_index = numberizer.numberize(k)

	print "computing gradient wrt %s" % k

	for i in xrange(hidden_dims):  # loop on hidden units, compute_g for each
		g = compute_gradients(rnn.lastval[-2][i])
		wx = g[in_history[index]]
		di_dk.append([numpy.dot(in_weight[index][j].value, wx) for j in xrange(len(vocab))]) 
	
	"""
	4. Populate txt file with matrix where M[i,k] is how strongly word k affects unit i
	"""
	print "writing %s" % k
	for h_i in di_dk:
		out_data.append(h_i[w_index])
	f.write("%s " % k)	
	for word in out_data:
		f.write("%s " % word)
	f.write("\n")

f.close()





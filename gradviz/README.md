#Creating Heatmaps with Ease

This directory contains all the code needed to generate heatmaps visualizing the gradients of an RNN or LSTM. Currently the three 
files that can achieve this are:

1. gradient.py
    * Calculates gradient of last hidden layer wrt entire vocabulary
2. grad_sentence.py
    * Calculates gradient of last hidden layer wrt subset of vocab. (Don't use this one)
3. grad_inp-hist.py
    * Calculates gradient of last hidden layer wrt input history. A.k.a. how each word in the sentence affected each unit's prediction of the next token.
  
##How to run

The grad\*.py files do not create the heatmaps, they only generate a txt file used by heat_map.py. Thus creating the heatmaps is split 
into two parts.

###Generating data
The inputs in the grad\*.py files must be manually adjusted. Currently, all three files work with an rnnlm1.py that was trained on the 
Penn TreeBank. Another language model can be loaded by modifying section 1 in the grad\*.py files to be another one from the lm/
directory. This is currently not recommended.

The input text to be fed into the language model can be found under section 2 in any of the grad\*.py files. The output file is also
named in this section. Modify these sections to change the input to and output from the network. 

rnnlm1 is trained on sentences, so 
make sure the length of the input text does not exceed one sentence. Each line of the train and valid texts are one sentence, ending in a 
<\s> token.

To generate data from grad_inp-hist.py for example, simply run
```bash
python grad_inp-hist.py
```

###Creating heatmap
The code to generate a heatmap is contained in heat_map.py. This file takes in a txt file as a command line argument, and generates
heatmap(s) in pdf format. The current form of the graphs have the hidden layers as the colunns, and the vocab as the rows. The cells
are shaded blue and red to correspond to positive and negative values, respectively.

The name of the output pdf is set on the last line of the file. To generate a heatmap from a text file "example.txt", simply run
```bash
python heat_map.py example.txt
```
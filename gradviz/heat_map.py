import sys
sys.path.append("..")
import matplotlib.pyplot as plt
import matplotlib.colors
import numpy
import fileinput
from penne import lm 

data = []
words = []
sentence = []
vocab = lm.make_vocab(lm.read_data("../data/ptb.train.txt"))
numberizer = lm.Numberizer(vocab)

for i in xrange(len(vocab)):
	data.append(numberizer.denumberize(i))

for line in list(fileinput.input()):
	weights = line.split()
	sentence.append(weights.pop(0))
	words.append(numpy.array(map(float, weights)))  # words is now a 2d array of weights
words = words[::-1]
sentence = sentence[::-1]
words = numpy.array(words)

def make_heatmap(data, height, width, ylabels, xlabels, norm=None):
	if norm is None:
		range = numpy.mean(data**2)**0.5 * 12**0.5 / 2
		norm = matplotlib.colors.Normalize(vmin=-range, vmax=range)
	fig, ax = plt.subplots()
	fig.set_size_inches(width, height)
	heatmap = ax.pcolor(data, norm=norm, cmap=plt.cm.RdBu)
	ax.yaxis.set_tick_params(left=False, right=False, labelleft=True, labelright=True)
	ax.set_yticks(numpy.arange(data.shape[0])+0.5, minor=False)
	ax.set_yticklabels(ylabels, minor=False, fontsize=6)
	ax.invert_yaxis()
	ax.xaxis.set_tick_params(top=False, bottom=False, labeltop=True, labelbottom=True)
	ax.set_xticks(numpy.arange(data.shape[1])+0.5, minor=False)
	ax.set_xticklabels(xlabels, minor=False, fontsize=6)

n_pages = 10
pagesize = 50

for i in xrange(0, min(n_pages*pagesize, len(sentence)), pagesize):
	make_heatmap(words[i:i+pagesize], 9., 6., sentence[i:i+pagesize], numpy.arange(words.shape[1])+1)
	plt.savefig("inp-hist_1_-1-%03d.pdf" % (i/pagesize))

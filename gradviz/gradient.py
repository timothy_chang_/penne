"""
Graph over the entire vocabulary

Deep recurrent language model. This one stacks RNNs using the
recurrent.Stacked class, which is analogous to composition of
finite-state transducers.
"""


"""
1. Loads rnnlm1.py
"""
import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
from penne import compute
from penne import expr
import operator
#import penne
import numpy
import random
import time

#use_gpu('cuda0')

hidden_dims = 100  # like the paper
depth = 2

#train = lm.read_data("../data/inferno.en")#[:100]
train = lm.read_data("../data/ptb.train.txt")[:70]
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")#[:100]
valid = lm.read_data("../data/ptb.valid.txt")[:70]

layers = [recurrent.Map(Layer(-len(vocab), hidden_dims, f=None, bias=None))]
for i in xrange(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stack(*layers)

def make_network(words):
    loss = constant(0.)
    prev_w = numberizer.numberize("<s>")
    rnn.start()
    for word in words:
        w = numberizer.numberize(word)
        o = rnn.step(prev_w)
        loss -= o[w]
        prev_w = w
    return loss

load_model("rnnlm1.npy")

"""
2. Run lm on input up to a sentence
"""
loss = make_network(valid[0][1])

"""
3. Calculates gradient of h_i wrt the entire vocabulary
"""
a = []
# loop on hidden units, compute_g for each
for i in xrange(hidden_dims):
	g = compute_gradients(rnn.lastval[-2][i])  # -2 denotes last hidden layer
	# dot(w,g(wx[k])) where w is w of input layer (layers[0].weight[0])
	fweight = layers[0].f.weight[0]  # weight[0] is list
	wx = g[layers[0].f.wx[0]]
	a.append([numpy.dot(fweight[j].value, wx) for j in xrange(len(vocab))])  # a list of matrices of stuff...

data = [[None for i in xrange(len(a))] for j in xrange(len(vocab))]

f = open("heat_map_norm_10e6.txt", "w")
for i in xrange(len(a)):  # over all hidden units
	# sort_a = sorted(enumerate(a[i]), key=operator.itemgetter(1))  # sort array of words by value
	# rev_a = sorted(sort_a, key=operator.itemgetter(1), reverse=True)
	for k in xrange(len(vocab)):  # over each word in vocab
		# print numberizer.denumberize(sort_a[k][0]), numpy.log(-1./(sort_a[k][1]))
		# f.write("%s %s\n" % (numberizer.denumberize(sort_a[k][0]), numpy.log(-1./(sort_a[k][1]))))  # word and score for unit i
		# weight = numpy.log(1./(a[i][k]))
		# if numpy.isnan(weight):
			# weight = 0.
		data[k][i] = a[i][k] * 1000000
	# print "bottom 10"
	# for k in xrange(10):
		# print numberizer.denumberize(rev_a[k][0]), rev_a[k][1]

for row in data:
	for word in row:
		f.write("%s " % word)
	f.write("\n")
f.close()





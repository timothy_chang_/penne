"""
DO NOT USE
Graph over specific words in vocab.
Deep recurrent language model. This one stacks RNNs using the
recurrent.Stacked class, which is analogous to composition of
finite-state transducers.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
from penne import compute
from penne import expr
import operator
#import penne
import numpy
import random
import time

hidden_dims = 100  # like the paper
depth = 1

#train = lm.read_data("../data/inferno.en")#[:100]
train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")#[:100]
valid = lm.read_data("../data/ptb.valid.txt")

layers = [recurrent.Map(Layer(-len(vocab), hidden_dims, f=None, bias=None))]
for i in xrange(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stack(*layers)

def make_network(words):
    loss = constant(0.)
    prev_w = numberizer.numberize("<s>")
    rnn.start()
    for word in words:
        w = numberizer.numberize(word)
        o = rnn.step(prev_w)
        loss -= o[w]
        prev_w = w
    return loss

learning_rate = 0.1

print "loading model"
load_model("rnnlm1_1_ptb.npy")
print "done loading model"

nwords = 50

f = open("sentence_0.txt", "w")
loss = make_network(valid[0])  # first line
for k in valid[5]:
	# run network on first word of validation set
	a = []
	data = []
	# loop on hidden units, compute_g for each
	print "computing gradient wrt %s" % k
	for i in xrange(hidden_dims):
		g = compute_gradients(rnn.lastval[-2][i])  # lastval is h

		# dot(w,g(wx[k])) where w is w of input layer (layers[0].weight[0])
		fweight = layers[0].f.weight[0]  # weight "matrix" (or something) of input layer
		# a for loop for each wx in time
		wx = g[layers[0].f.wx[0]]  # input vector dotted with input weight
		a.append([numpy.dot(fweight[j].value, wx) for j in xrange(len(vocab))])  # This is what each unit thinks of all the words
	
	# cur_word = k
	print "writing %s" % k
	for val in a:
		data.append(val[numberizer.numberize(k)])
	f.write("%s " % k)	
	for word in data:
		f.write("%s " % word)
	f.write("\n")
	# print "len of weights: ", len(data)

# possibility <number> <number> <number> ...
# Next <number> <number> <number> ...
# ...

f.close()





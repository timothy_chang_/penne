"""Compute values, gradients, and other things."""

__all__ = ['compute_values', 'compute_value', 'compute_gradients', 'check_gradients']

import expr # parameter, topological
import debug

import backend as numpy
import sys
import time
import collections

### Forward and backward propagation

def format_value(v, indent=4):
    result = []
    newline = '\n' + ' '*indent
    s = repr(v).split('\n')
    if len(s) > 1: result.append(newline)
    result.append(newline.join([]+s))
    result.append('\n')
    return ''.join(result)
    
def compute_values(x, initvalues={}):
    """Evaluate an expression and all its subexpressions.

    x:          The expression to evaluate.
    initvalues: Optional dictionary from subexpressions to
                precomputed values; can be used to continue a
                computation when the expression grows.
    """
    values = collections.OrderedDict()

    for subx in expr.topological(x):
        if subx in initvalues:
            values[subx] = initvalues[subx]
        else:
            try:

                if debug.profile:
                    start_time = time.clock()

                subx.forward(values)

                if debug.profile:
                    stop_time = time.clock()

            except:
                if debug.stack:
                    sys.stderr.write("Expression traceback (most recent call last):\n" + "".join(debug.format_list(subx.stack)))
                raise

            if debug.trace:
                sys.stdout.write("<%s> = %s = %s" % (subx.serial, subx, format_value(values[subx])))

            if debug.profile:
                filename, lineno, _, _ = subx.stack[1]
                name = "%s:%s" % (filename, lineno)
                #name = str(subx.__class__.__name__)
                debug.profile_counts[name] += 1
                debug.profile_times[name] += stop_time-start_time

    return values

if debug.profile:
    import atexit
    def print_profile():
        lines = [(value, line) for line, value in debug.profile_times.iteritems()]
        lines.sort(reverse=True)

        sys.stdout.write("  ncalls      time  location\n")
        for _, line in lines:
            sys.stdout.write("%8d  %8.3f  %s\n" % (debug.profile_counts[line], debug.profile_times[line], line))
    atexit.register(print_profile)

def compute_value(x):
    """Evaluate an expression."""
    return compute_values(x)[x]

def compute_gradients(x, values=None, compute_all=False):  # new addition: compute_all. computes all gradients.
    """Compute gradients using automatic differentiation.

    x:      The expression to compute gradients of.
    values: As returned by compute_values(x); contains all values to compute
            gradients with respect to.
    compute_all: Compute gradient with respect to everything.
    """

    if values is None:
        values = compute_values(x)
    if not isinstance(values, collections.OrderedDict): raise TypeError()
    if x not in values: raise ValueError()

    gradients = {x: 1.}
    for subx in values:
        if compute_all or isinstance(subx, expr.parameter) or any(arg in gradients for arg in subx.args):  # if !compute_all, only compute graident if subx is a parameter(H or b), or if any argument of the subx needs a gradient
            if subx not in gradients:
                gradients[subx] = 0.

    for subx in reversed(values): # assume values is in topological order
        if subx not in gradients:
            continue
	if debug.trace:
	    sys.stdout.write("d<%s>/d<%s> = %s" % (x.serial, subx.serial, format_value(gradients[subx])))
            
        try:
            if debug.profile:
                start_time = time.clock()

            subx.backward(values, gradients)

            if debug.profile:
                stop_time = time.clock()

        except:
            if debug.stack:
                sys.stderr.write("Expression traceback (most recent call last):\n" + "".join(debug.format_list(subx.stack)))
            raise

        if debug.trace:
            for arg in subx.args:
                if arg in gradients:
                    sys.stdout.write("    d<%s>/d<%s> := %s" % (x.serial, arg.serial, format_value(gradients[arg], indent=8)))

        if debug.profile:
            filename, lineno, _, _ = subx.stack[1]
            name = "%s:%s" % (filename, lineno)
            #name = str(subx.__class__.__name__)
            debug.profile_times[name] += stop_time-start_time

        if not isinstance(subx, expr.parameter):
            del gradients[subx]

    return gradients

def check_gradients(x, delta=0.01, threshold=0.01, params=None):
    """Compute gradients using symmetric differences.

    x:         Expression to compute gradients of.
    delta:     Size of change in parameter value.
    threshold: Maximum tolerated error.

    This is extremely slow and is used only for debugging."""

    if params is None:
        params = [subx for subx in expr.topological(x) if isinstance(subx, expr.parameter)]
    gradients = compute_gradients(x)

    for param in params:
        assert param in gradients
        g = numpy.empty_like(param.value)
        it = numpy.nditer([param.value, g], [], [['readwrite'], ['writeonly']])
        for pi, gi in it:
            save = pi
            pi -= delta/2
            val_minus = compute_value(x)
            pi += delta
            val_plus = compute_value(x)
            assert val_minus.ndim == val_plus.ndim == 0
            pi[...] = save
            gi[...] = (val_plus-val_minus)/delta
        d = (g - gradients[param]).ravel()
        error = (numpy.dot(d, d)/d.size) ** 0.5
        if error > threshold:
            if debug.stack:
                sys.stderr.write("Expression traceback (most recent call last):\n" + "".join(debug.format_list(param.stack)))
            sys.stderr.write("gradient (finite difference): %s\n" % g)
            sys.stderr.write("gradient (automatic): %s\n" % gradients[param])
            raise ValueError()


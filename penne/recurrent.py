"""Recurrent neural networks as finite-state transducers."""

# To do:
# Allow multiple inputs as with make_layer?

from expr import *
from nn import *
import backend as numpy
# import numpy as np

class Transducer(object):
    """Base class for transducers."""

    def state(self):
        return None
    def start(self, state=None):       pass
    def start_batch(self, size):       pass
    def start_batch_member(self, num): pass

    def transduce(self, inps):
        """Apply transducer to a sequence of input symbols."""

        self.start()
        outputs = []
        for inp in inps:
            outputs.append(self.step(inp))
        return outputs

    def transduce_batch(self, batch, parallel=None):
        """Apply transducer to a batch of sequences of input symbols.

        batch:    A list of lists of input symbols.
        parallel: How many sequences to run in parallel (default all).

        If the sequences are of unequal length, it's better for
        len(batch) to be several times larger than parallel to keep
        all cylinders firing.
        """

        if parallel is None: parallel = len(batch)

        outbatch = [[] for xs in batch]

        # Sort batch by length
        q = range(len(batch))
        q.sort(key=lambda i: len(batch[i]))

        # Set up data structures to keep track of strings to process in parallel
        # If running[num] = [i, j], then next symbol is batch[i][j]
        running = {}
        inp = []
        num = 0
        while len(running) < parallel and len(q) > 0:
            running[num] = [q.pop(), 0]
            inp.append(None)
            num += 1
        self.start_batch(len(inp))

        while len(running) + len(q) > 0:
            for num in xrange(parallel):
                if num not in running: continue
                i, j = running[num]
                if j == len(batch[i]):
                    if len(q) > 0:
                        running[num] = [i, j] = [q.pop(), 0]
                        self.start_batch_member(num)
                    else:
                        del running[num]
                        continue
                inp[num] = batch[i][j]
                running[num][1] += 1
            out = self.step(inp)
            for num, (i, j) in running.iteritems():
                outbatch[i].append(out[num])

        return outbatch

class Map(Transducer):
    """Stateless transducer that just applies a function to every symbol."""

    def __init__(self, f):
        self.f = f
    def step(self, inp):
        return self.f(inp)

class Stack(Transducer):
    """Several stacked recurrent networks, or, the composition of several FSTs."""

    def __init__(self, *layers):
        self.layers = layers
	self.lastval = []

    def state(self):
        return [layer.state() for layer in self.layers]

    def start(self, state=None):
        for i, layer in enumerate(self.layers):
            layer.start(state[i] if state is not None else None)

    def start_batch(self, size):
        for layer in self.layers:
            layer.start_batch(size)
    def start_batch_member(self, number):
        for layer in self.layers:
            layer.start_batch_member(number)

    def step(self, inp):
        val = inp
	# new addition:
	# lastval keeps track of each input
	# self.lastval.append(inp)
	del self.lastval[:]
        for layer in self.layers:
	    # why is lastval important?
	    # self.lastval[] - output of layer
            val = layer.step(val)
	    self.lastval.append(val)
        return val

class Simple(Transducer):
    """Simple (Elman) recurrent network.

    input_dims:  Number of input units.
    output_dims: Number of output units.
    f:           Activation function (default tanh)
    """

    def __init__(self, input_dims, output_dims, f=tanh, model=parameter.all):
        dims = [input_dims, output_dims]
        self.layer = Layer(dims, output_dims, f=f, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def state(self):
        return self.h
        
    def start(self, state=None):
        if state is None:
            self.h = self.h0
        else:
            self.h = h

    def start_batch(self, size):
        self.h = stack([self.h0]*size)
    def start_batch_member(self, number):
        self.h = setitem(self.h, number, self.h0)

    def step(self, inp):
        """inp can be either a vector Expression or an int """
        self.h = self.layer(inp, self.h)
        return self.h

# FOFE questions
# self.h - what is this and where did it come from?
# Are any of these other functions relevant?
# What makes this unit recurrent?

class FOFE(Transducer):
    """FOFE unit.  I want this unit to perform a scalar multiplication on z_t, the partial FOFE sequence, and add it to e_t, the 1-hot represenation of the current word.

    input_dims:  Number of input units.
    output_dims: Number of output units.
    f:           Activation function (default none)
    """

    def __init__(self, input_dims, output_dims, alpha, f=tanh, model=parameter.all):  # I don't want any function here.
        # dims = [input_dims, output_dims]
        # self.input_dims = input_dims
        # self.layer = Layer(dims, output_dims, f=f, model=model)
        # self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?  
        self.alpha = constant(alpha)

        self.weight = parameter(numpy.random.uniform(-0.1, 0.1, (-input_dims, output_dims)))
        self.bias = parameter(numpy.zeros(output_dims))
        self.activation = f


    def state(self):
        return self.h  # self.h? The weight matrix?
        
    def start(self, state=None):
        # if state is None:
        #     self.h = self.h0
        # else:
        #     self.h = h
        self.h = self.bias
        

    def step(self, word):
        """word can be either a vector Expression or an int """  
        # self.h = self.layer(inp, self.h)  # we want this call, instead of doing all the cool things in the __call__ part of Layer(), to perform the a*z_t-1 + e_t function.
        # if (self.input_dims < 0):
            # word = one_hot(-self.input_dims,word)
        # self.h = self.alpha*self.h + inp
        
        # The problem is with the type
        self.h = self.alpha*self.h + self.weight[word] + (constant(1)-self.alpha)*self.bias
        
        if self.activation:
            return self.activation(self.h)
        else:
            return self.h

        # self.y = alpha * y+ weight[word] + (1-alpha)*self.bias // 




class GRU(Transducer):
    """Gated recurrent unit.

    Cho et al., 2014. Learning phrase representations using RNN
    encoder-decoder for statistical machine translation. In
    Proc. EMNLP.
    """

    def __init__(self, input_dims, output_dims, model=parameter.all):
        dims = [input_dims, output_dims]
        self.reset_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        self.update_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        self.input_layer = Layer(dims, output_dims, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def start(self, state=None):
        if state is None:
            self.h = self.h0
        else:
            self.h = state

    def start_batch(self, size):
        self.h = stack([self.h0]*size)
    def start_batch_member(self, number):
        self.h = setitem(self.h, number, self.h0)

    def step(self, inp):
        r = self.reset_gate(inp, self.h)
        z = self.update_gate(inp, self.h)
        h_tilde = self.input_layer(inp, r*self.h)
        self.h = (constant(1.)-z)*self.h + z*h_tilde
        return self.h

class LSTM(Transducer):
    """Long short-term memory recurrent network.

    This version is from: Alex Graves, "Generating sequences with
    recurrent neural networks." arXiv:1308.0850.

    input_dims:  Number of input units.
    output_dims: Number of output units.
    f:           Activation function (default tanh)
    """

    def __init__(self, input_dims, output_dims, model=parameter.all):
        dims = [input_dims, output_dims, "diag"]
        self.input_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        self.forget_gate = Layer(dims, output_dims, f=sigmoid, bias=5., model=model)
        self.output_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        self.input_layer = Layer(dims[:-1], output_dims, f=tanh, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?
        self.c0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def state(self):
        return (self.h, self.c)

    def start(self, state=None):
        if state is None:
            self.h = self.h0
            self.c = self.c0
        else:
            self.h, self.c = state

    def start_batch(self, size):
        self.h = stack([self.h0]*size)
        self.c = stack([self.c0]*size)
    def start_batch_member(self, number):
        self.h = setitem(self.h, number, self.h0)
        self.c = setitem(self.c, number, self.c0)

    def step(self, inp):
        i = self.input_gate(inp, self.h, self.c)
        f = self.forget_gate(inp, self.h, self.c)
        #f = constant(1.) - i
        self.c = f * self.c + i * self.input_layer(inp, self.h)
        o = self.output_gate(inp, self.h, self.c)
        self.h = o * tanh(self.c)
        return self.h

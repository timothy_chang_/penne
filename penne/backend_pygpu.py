"""NumPy-like wrapper around libgpuarray/pygpu.

Much of this is from pygpu's _array.py, elemwise.py, and reduction.py.
"""

# bugs: x ** 0.5 becomes pow(float, double) which CUDA doesn't have

import pygpu
import pygpu.tools
import pygpu.blas
import numpy

convert_double_to_float = True

class ndarray(pygpu.gpuarray.GpuArray):
    def __str__(self):
        return str(numpy.asarray(self))
    def __repr__(self):
        return repr(numpy.asarray(self))

    def __pos__(self):         return self.copy()
    def __neg__(self):         return negative(self)
    def __abs__(self):         return absolute(self)

    def __add__(self, other):  return add(self, other)
    def __radd__(self, other): return add(other, self)
    def __iadd__(self, other): return add(self, other, out=self)
    def __sub__(self, other):  return subtract(self, other)
    def __rsub__(self, other): return subtract(other, self)
    def __isub__(self, other): return subtract(self, other, out=self)
    def __mul__(self, other):  return multiply(self, other)
    def __rmul__(self, other): return multiply(other, self)
    def __imul__(self, other): return multiply(self, other, out=self)
    def __div__(self, other):  return divide(self, other)
    def __rdiv__(self, other): return divide(other, self)
    def __idiv__(self, other): return divide(self, other, out=self)
    def __pow__(self, other):  return power(self, other)
    def __rpow__(self, other): return power(other, self)
    def __ipow__(self, other): return power(self, other, out=self)

    def __eq__(self, other):   return equal(self, other)
    def __ne__(self, other):   return not_equal(self, other)
    def __lt__(self, other):   return less(self, other)
    def __le__(self, other):   return less_equal(self, other)
    def __gt__(self, other):   return greater(self, other)
    def __ge__(self, other):   return greater_equal(self, other)

    def __float__(self):
        if self.size != 1:
            raise TypeError("only length-1 arrays can be converted to Python scalars")
        return float(self.__array__())

    def fill(self, value):
        self[...] = value

    @property
    def T(self):
        return self.transpose()

### Creation

def _demote_type(dtype):
    if dtype in [float, numpy.float64, numpy.float128]:
        return numpy.float32
    if dtype in [int, numpy.int64]:
        return numpy.int32
    return dtype

def result_type(*args):
    dtype = numpy.result_type(*args)
    if convert_double_to_float:
        dtype = _demote_type(dtype)
    return dtype

def array(proto):
    if isinstance(proto, float):
        proto = numpy.asarray(proto)
    if convert_double_to_float:
        proto = proto.astype(_demote_type(proto.dtype))
    return pygpu.array(proto, cls=ndarray)

def asarray(proto):
    if isinstance(proto, ndarray):
        return proto
    else:
        return array(proto)

def empty(shape, dtype=float):
    if convert_double_to_float:
        dtype = _demote_type(dtype)
    return pygpu.empty(shape, dtype=dtype, cls=ndarray)
def empty_like(a, dtype=None):
    return empty(a.shape, dtype=dtype or a.dtype)

def zeros(shape, dtype=float):
    if convert_double_to_float:
        dtype = _demote_type(dtype)
    return pygpu.zeros(shape, dtype=numpy.dtype(dtype), cls=ndarray)
def zeros_like(a, dtype=None):
    return zeros(a.shape, dtype=dtype or a.dtype)

def full(shape, fill_value, dtype=None):
    if dtype is None:
        dtype = numpy.array(fill_value).dtype
    if convert_double_to_float:
        dtype = _demote_type(dtype)
    a = pygpu.empty(shape, dtype=dtype, cls=ndarray)
    a.fill(fill_value)
    return a
def full_like(a, fill_value, dtype=None):
    return full(a.shape, fill_value, dtype=dtype or a.dtype)

class _Random(object):
    @staticmethod
    def uniform(low, high, size, dtype=float):
        return array(numpy.random.uniform(float(low), float(high), size).astype(dtype))

    @staticmethod
    def normal(loc, scale, size, dtype=float):
        return array(numpy.random.normal(float(loc), float(scale), size).astype(dtype))

random = _Random()

### Shape

def shape(a):              return asarray(a).shape
def ndim(a):               return asarray(a).ndim
def reshape(a, *params):   return a.reshape(*params)
def transpose(a, *params): return a.transpose(*params)

import pygpu.operations

def concatenate(arrays, axis=0):
    return asarray(pygpu.operations.concatenate(arrays, axis))

def stack(arrays, axis=0):
    if not arrays:
        raise ValueError('need at least one array to stack')

    shapes = set(arr.shape for arr in arrays)
    if len(shapes) != 1:
        raise ValueError('all input arrays must have the same shape')

    result_ndim = arrays[0].ndim + 1
    if not -result_ndim <= axis < result_ndim:
        msg = 'axis {0} out of bounds [-{1}, {1})'.format(axis, result_ndim)
        raise IndexError(msg)
    if axis < 0:
        axis += result_ndim

    shape = arrays[0].shape[:axis] + (1,) + arrays[0].shape[axis:]
    expanded_arrays = [arr.reshape(shape) for arr in arrays]
    return concatenate(expanded_arrays, axis=axis)

### Elementwise operations

## Unary

@pygpu.tools.lfu_cache(1000)
def get_elemwise1(context, op, a_arg, odtype):
    """Returns a unary elementwise kernel.

    context: GpuContext
    op: str, a C expression written in terms of {0}
    a_arg: ArrayArg
    odtype: output dtype
    """
    oper = "res[i] = " + op.format(a_arg.expr())
    args = [a_arg, pygpu.tools.ArrayArg(odtype, 'res')]
    return pygpu.elemwise.ElemwiseKernel(context, args, oper, dimspec_limit=1000, spec_limit=1000)

def elemwise1(op, a, out=None, out_dtype=None):
    a = asarray(a)
    if out is None:
        out = a._empty_like_me(dtype=out_dtype)
    k = get_elemwise1(a.context, 
                      op, pygpu.tools.as_argument(a, 'a'), 
                      out.dtype)
    k(a, out)
    return out

def absolute(a, out=None):
    if a.dtype.kind == 'u':
        return a.copy()
    elif a.dtype.kind == 'f':
        op = "fabs({0})"
    elif a.dtype.itemsize < 4:
        # cuda 5.5 finds the c++ stdlib definition if we don't cast here.
        op = "abs((int){0})"
    else:
        op = "abs({0})"
    return elemwise1(op, a, out)
def fabs(a, out=None):     return elemwise1("fabs({0})",   a, out)

def negative(a, out=None): return elemwise1("-{0}",       a, out)

def log(a, out=None):      return elemwise1("log({0})",   a, out)
def log2(a, out=None):     return elemwise1("log2({0})",  a, out)
def log10(a, out=None):    return elemwise1("log10({0})", a, out)
def log1p(a, out=None):    return elemwise1("log1p({0})", a, out)
def exp(a, out=None):      return elemwise1("exp({0})",   a, out)
def exp2(a, out=None):     return elemwise1("exp2({0})",  a, out)
def exp10(a, out=None):    return elemwise1("exp10({0})", a, out)
def expm1(a, out=None):    return elemwise1("expm1({0})", a, out)
def sqrt(a, out=None):     return elemwise1("sqrt({0})",  a, out)
def cbrt(a, out=None):     return elemwise1("cbrt({0})",  a, out)

def sin(a, out=None):      return elemwise1("sin({0})",   a, out)
def cos(a, out=None):      return elemwise1("cos({0})",   a, out)
def tan(a, out=None):      return elemwise1("tan({0})",   a, out)
def arcsin(a, out=None):   return elemwise1("asin({0})",  a, out)
def arccos(a, out=None):   return elemwise1("acos({0})",  a, out)
def arctan(a, out=None):   return elemwise1("atan({0})",  a, out)
def sinh(a, out=None):     return elemwise1("sinh({0})",  a, out)
def cosh(a, out=None):     return elemwise1("cosh({0})",  a, out)
def tanh(a, out=None):     return elemwise1("tanh({0})",  a, out)
def arcsinh(a, out=None):  return elemwise1("asinh({0})", a, out)
def arccosh(a, out=None):  return elemwise1("acosh({0})", a, out)
def arctanh(a, out=None):  return elemwise1("atanh({0})", a, out)

def sigmoid(a, out=None):  return elemwise1("1.0/(1.0+exp(-{0}))", a, out)

def xonemx(a, out=None): return elemwise1("{0}*(1.0-{0})", a, out)
def onemxx(a, out=None): return elemwise1("1.0-{0}*{0}", a, out)

## Binary

def _align(args):
    nd = 0
    context = cls = None
    for i, arg in enumerate(args):
        if isinstance(arg, pygpu.gpuarray.GpuArray):
            nd = _max(nd, arg.ndim)
            if context is None:
                context = arg.context
                cls = arg.__class__
            else:
                assert arg.context == context
        else:
            args[i] = numpy.asarray(arg)

    shp = [0]*nd

    for i, arg in enumerate(args):
        if isinstance(arg, pygpu.gpuarray.GpuArray):
            ashp = (1,)*(nd-arg.ndim) + arg.shape
            args[i] = args[i].reshape(ashp)
            for j in xrange(nd):
                shp[j] = _max(shp[j], ashp[j])
            
    return args, (shp, result_type(*args), context, cls)

@pygpu.tools.lfu_cache(1000)
def get_elemwise2(context, op, a_arg, b_arg, odtype):
    """Returns a binary elementwise kernel.

    context: 
    op: str, a C expression written in terms of {0} and {1}
    a_arg: ArrayArg
    b_arg: ArrayArg
    odtype: output dtype
    """

    oper = "res[i] = " + op.format(a_arg.expr(), b_arg.expr())
    args = [a_arg, b_arg, pygpu.tools.ArrayArg(odtype, 'res')]
    return pygpu.elemwise.ElemwiseKernel(context, args, oper, dimspec_limit=1000, spec_limit=1000)

@pygpu.tools.lfu_cache(1000)
def get_ielemwise2(context, op, a_arg, b_arg):
    args = [a_arg, b_arg]

    oper = a_arg.expr() + " = " + op.format(a_arg.expr(), b_arg.expr())
    
    return pygpu.elemwise.ElemwiseKernel(context, args, oper, dimspec_limit=1000, spec_limit=1000)

def elemwise2(op, a, b, out=None, out_dtype=None):
    (a, b), (out_shape, out_dtype1, context, cls) = _align([a, b])
    out_dtype = out_dtype or out_dtype1

    if out is None:
        out = pygpu.gpuarray.empty(out_shape, out_dtype, context=context, cls=cls)

    if out is a:
        k = get_ielemwise2(context, op,
                           pygpu.tools.as_argument(a, 'a'),
                           pygpu.tools.as_argument(b, 'b'))
        k(a, b, broadcast=True)
    else:
        k = get_elemwise2(context, op,
                          pygpu.tools.as_argument(a, 'a'),
                          pygpu.tools.as_argument(b, 'b'),
                          out.dtype)
        k(a, b, out, broadcast=True)
    return out

def add(a, b, out=None):      return elemwise2("{0}+{1}", a, b, out)
def subtract(a, b, out=None): return elemwise2("{0}-{1}", a, b, out)
def multiply(a, b, out=None): return elemwise2("{0}*{1}", a, b, out)
def divide(a, b, out=None):   return elemwise2("{0}/{1}", a, b, out)
def power(a, b, out=None):
    if b == 2:
        return elemwise1("{0}*{0}", a, out)
    if isinstance(b, (int, numpy.int64)):
        b = numpy.int32(b) # CUDA doesn't allow long int
    return elemwise2("pow({0},{1})", a, b, out)

def maximum(a, b, out=None):  return elemwise2("fmax({0},{1})", a, b, out)
def minimum(a, b, out=None):  return elemwise2("fmin({0},{1})", a, b, out)

def arctan2(a, b, out=None):  return elemwise2("atan2({0},{1})", a, b, out)
def hypot(a, b, out=None):    return elemwise2("hypot({0},{1})", a, b, out)

### Logical operations

def equal(a, b, out=None):          return elemwise2("{0} == {1}", a, b, out=out, out_dtype=numpy.bool)
def not_equal(a, b, out=None):      return elemwise2("{0} != {1}", a, b, out=out, out_dtype=numpy.bool)
def greater(a, b, out=None):        return elemwise2("{0} > {1}", a, b, out=out, out_dtype=numpy.bool)
def greater_equal(a, b, out=None):  return elemwise2("{0} >= {1}", a, b, out=out, out_dtype=numpy.bool)
def less(a, b, out=None):           return elemwise2("{0} < {1}", a, b, out=out, out_dtype=numpy.bool)
def less_equal(a, b, out=None):     return elemwise2("{0} <= {1}", a, b, out=out, out_dtype=numpy.bool)
def logical_and(a, b, out=None):    return elemwise2("{0} && {1}", a, b, out=out, out_dtype=numpy.bool)
def logical_or(a, b, out=None):     return elemwise2("{0} || {1}", a, b, out=out, out_dtype=numpy.bool)
def logical_not(a, out=None):       return elemwise1("!{0}", a, out=out, out_dtype=numpy.bool)

@pygpu.tools.lfu_cache(1000)
def get_elemwise3(context, op, a_arg, b_arg, c_arg, odtype):
    """Returns a ternary elementwise kernel.

    context: 
    op: str, a C expression written in terms of {0}, {1}, and {2}
    a_arg, b_arg, c_arg: ArrayArg
    odtype: output dtype
    """

    oper = "res[i] = " + op.format(a_arg.expr(), b_arg.expr(), c_arg.expr())
    args = [a_arg, b_arg, c_arg, pygpu.tools.ArrayArg(odtype, 'res')]
    return pygpu.elemwise.ElemwiseKernel(context, args, oper, dimspec_limit=1000, spec_limit=1000)

def elemwise3(op, a, b, c, out=None, out_dtype=None):
    (a, b, c), (out_shape, out_dtype1, context, cls) = _align([a, b, c])
    out_dtype = out_dtype or out_dtype1

    if out is None:
        out = pygpu.gpuarray.empty(out_shape, out_dtype, context=context, cls=cls)

    k = get_elemwise3(context, op,
                      pygpu.tools.as_argument(a, 'a'),
                      pygpu.tools.as_argument(b, 'b'),
                      pygpu.tools.as_argument(c, 'c'),
                      out.dtype)
    k(a, b, c, out, broadcast=True)
    return out

def where(cond, a, b, out=None):
    return elemwise3("{0} ? {1} : {2}", cond, a, b, out=out, out_dtype=result_type(a, b))

### Reductions

def _redux(nd, axis):
    if axis is None:
        redux = [True] * nd
    else:
        redux = [False] * nd

        if not isinstance(axis, (list, tuple)):
            axis = (axis,)

        for ax in axis:
            if ax < 0:
                ax += nd
            if ax < 0 or ax >= nd:
                raise ValueError('axis out of bounds')
            redux[ax] = True
    return redux

@pygpu.tools.lfu_cache(1000)
def get_reduce1(context, op, a_arg, neutral, redux, odtype):
    reduce_expr = op.format("a", "b")
    return pygpu.reduction.ReductionKernel(context, dtype_out=odtype, neutral=neutral,
                                           reduce_expr=reduce_expr, redux=redux,
                                           arguments=[a_arg])

def reduce1(op, a, neutral, axis=None, keepdims=False, out=None):
    a = asarray(a)
    if a.ndim == 0:
        assert axis is None
        a = a.reshape((1,))
    redux = tuple(_redux(a.ndim, axis))
    shape0 = tuple(d for i, d in enumerate(a.shape) if not redux[i])
    shape1 = tuple(d if not redux[i] else 1 for i, d in enumerate(a.shape))

    if out is None:
        out = pygpu.gpuarray.empty(shape1 if keepdims else shape0, a.dtype, context=a.context, cls=a.__class__)
    if keepdims:
        out1 = out
        out0 = out1.reshape(shape0)
    else:
        out0 = out

    k = get_reduce1(a.context, 
                    op, pygpu.tools.as_argument(a, 'a'), neutral, 
                    redux,
                    out0.dtype)
    k(a, out=out0)
    return out

def asum(a, axis=None, keepdims=False, out=None):
    return reduce1("{0}+{1}", a, 0., axis, keepdims, out)
sum = asum

def amax(a, axis=None, keepdims=False, out=None):
    neutral = numpy.iinfo(a.dtype).min if issubclass(a.dtype.type, numpy.integer) else "-1.0/0.0"
    return reduce1("fmax({0},{1})", a, neutral, axis, keepdims, out)
_max = max
max = amax

def amin(a, axis=None, keepdims=False, out=None):
    neutral = numpy.iinfo(a.dtype).max if issubclass(a.dtype.type, numpy.integer) else "1.0/0.0"
    return reduce1("fmin({0},{1})", a, neutral, axis, keepdims, out)
min = amin

# to do: do division first as map
def mean(a, axis=None, keepdims=False, out=None):
    a = asarray(a)
    # numpy.mean casts integral types to float64
    if issubclass(a.dtype.type, (numpy.integer, numpy.bool_)) and out is None:
        a = a.astype(numpy.float64)
    out = sum(a, axis, keepdims, out)
    out /= a.size/out.size
    return out

@pygpu.tools.lfu_cache(1000)
def get_reduce2(context, map_op, reduce_op, a_arg, b_arg, neutral, redux, odtype):
    map_expr = map_op.format(a_arg.expr(), b_arg.expr())
    reduce_expr = reduce_op.format("a", "b")
    if odtype is None:
        odtype = result_type(a_arg.dtype, b_arg.dtype)

    return pygpu.reduction.ReductionKernel(context, dtype_out=odtype, neutral=neutral,
                                           map_expr=map_expr,
                                           reduce_expr=reduce_expr, redux=redux,
                                           arguments=[a_arg, b_arg])

def reduce2(map_op, reduce_op, a, b, neutral, axis=None, keepdims=False, out=None):
    (a, b), (out_shape, out_dtype, context, cls) = _align([a, b])
    redux = tuple(_redux(len(out_shape), axis))
    shape0 = tuple(d for i, d in enumerate(out_shape) if not redux[i])
    shape1 = tuple(d if not redux[i] else 1 for i, d in enumerate(out_shape))

    if out is None:
        out = pygpu.gpuarray.empty(shape1 if keepdims else shape0, out_dtype, context=context, cls=cls)
    if keepdims:
        out1 = out
        out0 = out1.reshape(shape0)
    else:
        out0 = out

    k = get_reduce2(context, 
                    map_op, reduce_op,
                    pygpu.tools.as_argument(a, 'a'), pygpu.tools.as_argument(b, 'b'), neutral,
                    redux,
                    out0.dtype)
    k(a, b, out=out0)
    return out

### Linear algebra

def _dot(a, b, beta, out=None):
    out_dtype = result_type(a, b)

    if a.dtype != out_dtype:
        a = a.astype(out_dtype)
    if b.dtype != out_dtype:
        b = b.astype(out_dtype)
    if out is not None and out.dtype != out_dtype:
        raise TypeError("output array is not of correct dtype")

    if a.ndim == 2 and b.ndim == 1:
        if out is None:
            out = empty((a.shape[0],), out_dtype)
        pygpu.blas.gemv(1., a, b, beta, out, overwrite_y=True)

    elif a.ndim == 1 and b.ndim == 2:
        if out is None:
            out = empty((b.shape[1],), out_dtype)
        pygpu.blas.gemv(1., b, a, beta, out, trans_a=True, overwrite_y=True)

    elif a.ndim == b.ndim == 2:
        if out is None:
            out = empty((a.shape[0], b.shape[1]), out_dtype)
        pygpu.blas.gemm(1., a, b, beta, out, overwrite_c=True)

    elif a.ndim == a.ndim == 1:
        if out is None:
            out = empty((), out_dtype)
        out[...] = beta*out + reduce2("{0}*{1}", "{0}+{1}", a, b, 0., None, False)

    # to do: a.ndim > 2 or b.ndim > 2

    else:
        raise TypeError("unsupported combination of shapes")

    return out

def dot(a, b, out=None):
    if ndim(a) == 0 or ndim(b) == 0:
        return multiply(a, b, out)
    else:
        return asarray(_dot(a, b, 0., out))

def add_dot(a, b, out):
    if ndim(a) == 0 or ndim(b) == 0:
        out += a * b
        return out
    else:
        return _dot(a, b, 1., out)

def outer(a, b, out=None):
    out_dtype = result_type(a, b)
    if a.dtype != out_dtype:
        a = a.astype(out_dtype)
    if b.dtype != out_dtype:
        b = b.astype(out_dtype)
    if out is not None and out.dtype != out_dtype:
        raise TypeError("output array is not of correct dtype")

    if out is None:
        return array(pygpu.blas.ger(1., a, b)) # ger returns GpuArray
    else:
        out.fill(0.)
        pygpu.blas.ger(1., a, b, out, overwrite_a=True)
        return out

def add_outer(a, b, out):
    out_dtype = result_type(a, b)
    if a.dtype != out_dtype:
        a = a.astype(out_dtype)
    if b.dtype != out_dtype:
        b = b.astype(out_dtype)
    if out is not None and out.dtype != out_dtype:
        raise TypeError("output array is not of correct dtype")

    pygpu.blas.ger(1., a, b, out, overwrite_a=True)
    return out

def init_gpu(device="cuda0"):
    pygpu.set_default_context(pygpu.init(device))

